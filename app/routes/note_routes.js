module.exports = (app, db) => {

	app.get('/user/by-name/:fullName', (req, res) => {

		let query = {'fullName': { $regex: new RegExp(req.params.fullName, "i") } }

		db.collection("users").find(query).project({_id:0}).toArray( (err, item) => {
			if (err) {
				res.send({ 'error' : 'An error has occurred' })
			} else {
				res.send(item)
			}
		})
	})

	app.get('/book/by-title/:title', (req, res) => {
		let query = {'title': { $regex: new RegExp(req.params.title, "i") } }

		db.collection("books").find(query).project({_id:0}).toArray( (err, item) => {
			if (err) {
				res.send({ 'error' : 'An error has occurred' })
			} else {
				res.send(item)
			}
		})
	})

	app.get('/book/by-author/:author', (req, res) => {
		let query = {'author': { $regex: new RegExp(req.params.author, "i") } }

		db.collection("books").find(query).project({_id:0}).toArray( (err, item) => {
			if (err) {
				res.send({ 'error' : 'An error has occurred' })
			} else {
				res.send(item)
			}
		})
	})
	
}