const express  = require ('express')
const database = require('./config/db')
const mongoose = require('mongoose')
const app      = express()
const port     = process.env.PORT || 3000

mongoose.connect(database.url, (err, db) => {
	
	if (err) return console.log(err)

	app.use(function (req, res, next) {
	    res.setHeader('Access-Control-Allow-Origin', '*');
	    next();
	});

	require('./app/routes')(app, db);	
	app.listen(port, () => {
		console.log("Running on port: " + port);
	})

})