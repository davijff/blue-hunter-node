const database = require('./config/db')
const mongoose = require('mongoose')
const Schema = mongoose.Schema

mongoose.connect( database.url, (err, db) => {
	
	if (err) return console.log(err);

	const userSchema = new Schema({
		fullName: String,
		gender: String,
		age: Number,
		email: String,
		phone:String,
		username:String,
	})

	const usersData = [{"id":0,"fullName":"Nina Ellis","gender":"female","age":61,"email":"nEllis@example.com","phone":"(443) 545-8479","username":"nEllis"},{"id":1,"fullName":"Inez Peterson","gender":"female","age":61,"email":"iPeterson@example.com","phone":"(257) 202-5035","username":"iPeterson"},{"id":2,"fullName":"Nathan Cobb","gender":"male","age":39,"email":"nCobb@example.com","phone":"(368) 849-8721","username":"nCobb"},{"id":3,"fullName":"Nell Payne","gender":"female","age":49,"email":"nPayne@example.com","phone":"(754) 297-3282","username":"nPayne"},{"id":4,"fullName":"Juan Douglas","gender":"male","age":37,"email":"jDouglas@example.com","phone":"(950) 933-8532","username":"jDouglas"},{"id":5,"fullName":"Manuel Parker","gender":"male","age":34,"email":"mParker@example.com","phone":"(754) 381-4273","username":"mParker"},{"id":6,"fullName":"Benjamin Day","gender":"male","age":21,"email":"bDay@example.com","phone":"(360) 935-7443","username":"bDay"},{"id":7,"fullName":"Patrick Maldonado","gender":"male","age":58,"email":"pMaldonado@example.com","phone":"(204) 601-5789","username":"pMaldonado"},{"id":8,"fullName":"Jeff Smith","gender":"male","age":37,"email":"jSmith@example.com","phone":"(506) 360-8982","username":"jSmith"},{"id":9,"fullName":"Katherine Love","gender":"female","age":46,"email":"kLove@example.com","phone":"(679) 945-7785","username":"kLove"},{"id":10,"fullName":"Travis Bowen","gender":"male","age":60,"email":"tBowen@example.com","phone":"(528) 531-1611","username":"tBowen"},{"id":11,"fullName":"Samuel Hampton","gender":"male","age":43,"email":"sHampton@example.com","phone":"(913) 298-8410","username":"sHampton"},{"id":12,"fullName":"Kevin Norton","gender":"male","age":20,"email":"kNorton@example.com","phone":"(239) 237-6736","username":"kNorton"},{"id":13,"fullName":"Nicholas Ellis","gender":"male","age":27,"email":"nEllis@example.com","phone":"(813) 229-1864","username":"nEllis"},{"id":14,"fullName":"Louise Lucas","gender":"female","age":20,"email":"lLucas@example.com","phone":"(546) 345-1371","username":"lLucas"},{"id":15,"fullName":"Frances Perkins","gender":"female","age":23,"email":"fPerkins@example.com","phone":"(214) 885-5742","username":"fPerkins"},{"id":16,"fullName":"Esther Castillo","gender":"female","age":36,"email":"eCastillo@example.com","phone":"(541) 725-8912","username":"eCastillo"},{"id":17,"fullName":"Martha Gibson","gender":"female","age":59,"email":"mGibson@example.com","phone":"(739) 705-4110","username":"mGibson"},{"id":18,"fullName":"Ruby Harrington","gender":"female","age":26,"email":"rHarrington@example.com","phone":"(883) 524-1126","username":"rHarrington"},{"id":19,"fullName":"Lucille Curry","gender":"female","age":28,"email":"lCurry@example.com","phone":"(418) 496-5593","username":"lCurry"}];
	var User = db.model('User', userSchema);
	User.collection.insert(usersData, (err) => {
	 	if (err) console.log(err);
	})

	const bookSchema = new Schema({
		title: String,
		author: String,
		yearPublished: Number,
		price: String,
		rating: String
	})

	const booksData = [{"id":0,"title":"Culpa Enim Suscipit Voluptatem","author":"@cNewman","yearPublished":2015,"price":"$12.99","rating":"1.3/5"},{"id":1,"title":"Quasi Excepturi Mollitia","author":"@jNguyen","yearPublished":2006,"price":"$15.99","rating":"4.5/5"},{"id":2,"title":"Magnam Obcaecati Ab Asperiores A","author":"@fMcGee","yearPublished":2007,"price":"$20.99","rating":"4.1/5"},{"id":3,"title":"Ex","author":"@aDaniels","yearPublished":2005,"price":"$24.99","rating":"2.4/5"},{"id":4,"title":"Voluptate Cumque Qui Nihil","author":"@mSparks","yearPublished":2012,"price":"$8.99","rating":"1.6/5"},{"id":5,"title":"Laboriosam","author":"@eGuerrero","yearPublished":2010,"price":"$8.99","rating":"4.8/5"},{"id":6,"title":"Quidem Delectus","author":"@mHayes","yearPublished":2005,"price":"$20.99","rating":"4.1/5"},{"id":7,"title":"Voluptatibus Iure","author":"@tHolmes","yearPublished":2003,"price":"$14.99","rating":"3.2/5"},{"id":8,"title":"Aliquam Magnam Magni Voluptatem Dolore","author":"@eThornton","yearPublished":2016,"price":"$7.99","rating":"1.6/5"},{"id":9,"title":"Architecto Non Quasi Sed Consequatur","author":"@eKeller","yearPublished":2016,"price":"$12.99","rating":"2/5"},{"id":10,"title":"Iste Quod","author":"@mIngram","yearPublished":2005,"price":"$19.99","rating":"2/5"},{"id":11,"title":"Perspiciatis Quisquam","author":"@lHale","yearPublished":2012,"price":"$11.99","rating":"4.6/5"},{"id":12,"title":"Aperiam Iure","author":"@mStanley","yearPublished":2006,"price":"$9.99","rating":"2.1/5"},{"id":13,"title":"Quibusdam Similique Eius Earum Repudiandae","author":"@dLindsey","yearPublished":2001,"price":"$9.99","rating":"5/5"},{"id":14,"title":"Eaque Soluta","author":"@kWillis","yearPublished":2004,"price":"$10.99","rating":"2.7/5"},{"id":15,"title":"Eveniet Tempora Doloremque Aliquam","author":"@pDavidson","yearPublished":2010,"price":"$15.99","rating":"4.5/5"},{"id":16,"title":"Officia Ipsam Ea Soluta","author":"@tBishop","yearPublished":2008,"price":"$19.99","rating":"4.2/5"},{"id":17,"title":"Ducimus Repellat Sequi Accusamus Unde","author":"@mHunt","yearPublished":2010,"price":"$3.99","rating":"1/5"},{"id":18,"title":"Dicta Molestiae Perspiciatis Omnis Ducimus","author":"@mPratt","yearPublished":2002,"price":"$7.99","rating":"4.5/5"},{"id":19,"title":"Officiis Suscipit Quasi Sequi Error","author":"@mLove","yearPublished":2009,"price":"$12.99","rating":"2.9/5"}];
	var Book = db.model('Book', bookSchema);
	Book.collection.insert(booksData, (err) => {
	 	if (err) console.log(err);
	})
	
})

